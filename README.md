### Theme: ###
Study of the passage of harmonic signals through linear dynamical systems.
### Description: ###
Investigation of the linear dynamic systems when the input harmonic signals and methods for constructing dynamic filters.